class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.integer :appointment_id
      t.string :appointment_date
      t.string :appointment_time
      t.integer :patient_id
      t.integer :physician_id
      t.string :reason_for_appointment
      t.integer :diagnostic_id
      t.string :physician_note

      t.timestamps
    end
  end
end
