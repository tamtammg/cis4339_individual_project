class RemoveColumnsFromAppointments < ActiveRecord::Migration
  def change
    remove_column :appointments, :appointment_time, :string
    remove_column :appointments, :Appointment_Date_Time, :datetime
  end
end
