class AddColumnsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :office_worker, :boolean
    add_column :users, :physician, :boolean
  end
end
