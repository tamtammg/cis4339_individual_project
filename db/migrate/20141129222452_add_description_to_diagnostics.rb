class AddDescriptionToDiagnostics < ActiveRecord::Migration
  def change
    add_column :diagnostics, :diagnostic_description, :string
  end
end
