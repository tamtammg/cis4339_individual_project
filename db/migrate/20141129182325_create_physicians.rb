class CreatePhysicians < ActiveRecord::Migration
  def change
    create_table :physicians do |t|
      t.integer :physician_id
      t.string :physician_last_name
      t.string :physician_first_name

      t.timestamps
    end
  end
end
