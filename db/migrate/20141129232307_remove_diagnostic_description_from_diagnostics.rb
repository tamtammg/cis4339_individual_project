class RemoveDiagnosticDescriptionFromDiagnostics < ActiveRecord::Migration
  def change
    remove_column :diagnostics, :diagnostic_description, :string
  end
end
