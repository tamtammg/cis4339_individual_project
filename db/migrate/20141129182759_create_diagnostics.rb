class CreateDiagnostics < ActiveRecord::Migration
  def change
    create_table :diagnostics do |t|
      t.integer :diagnostic_id
      t.string :diagnostic_code
      t.decimal :diagnostic_charge

      t.timestamps
    end
  end
end
