class CreatePatients < ActiveRecord::Migration
  def change
    create_table :patients do |t|
      t.integer :patient_id
      t.string :patient_last_name
      t.string :patient_first_name

      t.timestamps
    end
  end
end
