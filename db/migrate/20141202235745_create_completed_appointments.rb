class CreateCompletedAppointments < ActiveRecord::Migration
  def change
    create_table :completed_appointments do |t|
      t.integer :appointment_id

      t.timestamps
    end
  end
end
