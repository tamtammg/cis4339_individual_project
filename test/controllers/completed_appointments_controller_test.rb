require 'test_helper'

class CompletedAppointmentsControllerTest < ActionController::TestCase
  setup do
    @completed_appointment = completed_appointments(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:completed_appointments)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create completed_appointment" do
    assert_difference('CompletedAppointment.count') do
      post :create, completed_appointment: { appointment_id: @completed_appointment.appointment_id }
    end

    assert_redirected_to completed_appointment_path(assigns(:completed_appointment))
  end

  test "should show completed_appointment" do
    get :show, id: @completed_appointment
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @completed_appointment
    assert_response :success
  end

  test "should update completed_appointment" do
    patch :update, id: @completed_appointment, completed_appointment: { appointment_id: @completed_appointment.appointment_id }
    assert_redirected_to completed_appointment_path(assigns(:completed_appointment))
  end

  test "should destroy completed_appointment" do
    assert_difference('CompletedAppointment.count', -1) do
      delete :destroy, id: @completed_appointment
    end

    assert_redirected_to completed_appointments_path
  end
end
