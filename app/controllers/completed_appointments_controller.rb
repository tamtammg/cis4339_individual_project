class CompletedAppointmentsController < ApplicationController
  before_action :set_completed_appointment, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @completed_appointments = CompletedAppointment.all
    respond_with(@completed_appointments)
  end

  def show
    respond_with(@completed_appointment)
  end

  def new
    @completed_appointment = CompletedAppointment.new
    respond_with(@completed_appointment)
  end

  def edit
  end

  def create
    @completed_appointment = CompletedAppointment.new(completed_appointment_params)
    @completed_appointment.save
    respond_with(@completed_appointment)
  end

  def update
    @completed_appointment.update(completed_appointment_params)
    respond_with(@completed_appointment)
  end

  def destroy
    @completed_appointment.destroy
    respond_with(@completed_appointment)
  end

  private
    def set_completed_appointment
      @completed_appointment = CompletedAppointment.find(params[:id])
    end

    def completed_appointment_params
      params.require(:completed_appointment).permit(:appointment_id)
    end
end
