json.array!(@physicians) do |physician|
  json.extract! physician, :id, :physician_id, :physician_last_name, :physician_first_name
  json.url physician_url(physician, format: :json)
end
