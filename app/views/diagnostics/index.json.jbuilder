json.array!(@diagnostics) do |diagnostic|
  json.extract! diagnostic, :id, :diagnostic_id, :diagnostic_code, :diagnostic_charge
  json.url diagnostic_url(diagnostic, format: :json)
end
