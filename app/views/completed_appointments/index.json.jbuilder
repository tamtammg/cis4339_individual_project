json.array!(@completed_appointments) do |completed_appointment|
  json.extract! completed_appointment, :id, :appointment_id
  json.url completed_appointment_url(completed_appointment, format: :json)
end
