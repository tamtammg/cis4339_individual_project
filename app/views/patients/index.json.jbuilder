json.array!(@patients) do |patient|
  json.extract! patient, :id, :patient_id, :patient_last_name, :patient_first_name
  json.url patient_url(patient, format: :json)
end
