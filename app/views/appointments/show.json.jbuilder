json.extract! @appointment, :id, :appointment_id, :appointment_date, :appointment_time, :patient_id, :physician_id, :reason_for_appointment, :diagnostic_id, :physician_note, :created_at, :updated_at
