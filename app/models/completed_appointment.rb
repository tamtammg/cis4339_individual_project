class CompletedAppointment < ActiveRecord::Base

  belongs_to :appointment
  has_many :diagnostics, through: :appointments
  has_many :patients, through: :appointments
  has_many :physicians, through: :appointments


end
