class Patient < ActiveRecord::Base

  has_many :appointments
  has_many :physicians, through: :appointments
  has_many :invoices, through: :appointments
  has_many :completed_appointments, through: :appointments
end
