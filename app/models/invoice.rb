class Invoice < ActiveRecord::Base
  belongs_to :appointment
  has_many :diagnostics, through: :appointment
  has_many :patients, through: :appointment
  has_many :physicians, through: :appointment
end
