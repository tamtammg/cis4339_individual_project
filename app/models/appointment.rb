class Appointment < ActiveRecord::Base

  belongs_to :physician
  belongs_to :patient
  belongs_to :diagnostic
  has_many :invoices
  has_many :completed_appointments


end
